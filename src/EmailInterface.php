<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use Psr\Http\Message\MessageInterface;

/**
 * EmailInterface interface file.
 * 
 * An Email is an HTTP message which is passed from server to server. This
 * interface defines what differs from common HTTP requests and responses.
 * 
 * Emails are considered immutable; all methods that might change state MUST
 * be implemented such that they retain the internal state of the current
 * message and return an instance taht contains the changed state.
 * 
 * @author Anastaszor
 */
interface EmailInterface extends EmailMetadataInterface, MessageInterface
{
	
	// nothing to add. Every other data should be included in the body
	// or in the headers
	
}
