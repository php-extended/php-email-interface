<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use DateTimeInterface;
use Stringable;

/**
 * EmailMetadataInterface interface file.
 * 
 * An EmailMetadata is an extract of the data of the full email, which can be
 * available when doing listing with an email provider.
 * 
 * EmailMetadatas are considered immutable; all methods that might change state
 * MUST be implemented such that they retain the internal state of the current
 * message and return an instance taht contains the changed state.
 * 
 * @author Anastaszor
 */
interface EmailMetadataInterface extends Stringable
{
	
	/**
	 * Retrieves the provider name.
	 * 
	 * The provider is a domain name that is owned by the provider, 
	 * or a specific string that refers directly or indirectly to it.
	 * 
	 * @return string the name of the provider
	 */
	public function getProviderName() : string;
	
	/**
	 * Returns an instance with the specified provider value.
	 * 
	 * @param string $providerName the provider of the message
	 * @return EmailMetadataInterface
	 */
	public function withProviderName(string $providerName) : EmailMetadataInterface;
	
	/**
	 * Retrieves the identifier of this message, given the provider.
	 * 
	 * The identifier of the message is a key to retrieve this specific message
	 * that is given to the provider to this specific message.
	 * 
	 * @return string
	 */
	public function getIdentifier() : string;
	
	/**
	 * Returns an instance with the specified identifier.
	 * 
	 * @param string $identifier if identifier of the message
	 * @return EmailMetadataInterface
	 */
	public function withIdentifier(string $identifier) : EmailMetadataInterface;
	
	/**
	 * Retrieves the origination date, which is the date when the creator of the
	 * message sent it.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getOrigDate() : ?DateTimeInterface;
	
	/**
	 * Returns an instance of the email with the given orig date.
	 * 
	 * @param DateTimeInterface $datetime
	 * @return EmailMetadataInterface
	 */
	public function withOrigDate(DateTimeInterface $datetime) : EmailMetadataInterface;
	
	/**
	 * Retrieves the from email address of this message. This data is taken
	 * out from the FROM header of the message.
	 * 
	 * @return MailboxListInterface
	 */
	public function getFrom() : MailboxListInterface;
	
	/**
	 * Returns an instance of the email mailbox list as From.
	 * 
	 * @param MailboxListInterface $list the email address of the sender
	 * @return EmailMetadataInterface
	 */
	public function withFrom(MailboxListInterface $list) : EmailMetadataInterface;
	
	/**
	 * Retrieves the from email address of this message. This data is taken
	 * out from the Sender header of the message if exists, and from the TO
	 * if it does not.
	 * 
	 * @return ?MailboxInterface
	 */
	public function getSender() : ?MailboxInterface;
	
	/**
	 * Returns an instance of the email with the given mailbox as Sender.
	 * 
	 * @param MailboxInterface $mailbox
	 * @return EmailMetadataInterface
	 */
	public function withSender(MailboxInterface $mailbox) : EmailMetadataInterface;
	
	/**
	 * Retrieves the Reply-To email address of this message. This data is takken
	 * out from the Reply-To header of the message if exists, and from the TO
	 * if it does not.
	 * 
	 * @return EmailAddressListInterface
	 */
	public function getReplyTo() : EmailAddressListInterface;
	
	/**
	 * Returns an instance of the email with the given address list as Reply-To.
	 * 
	 * @param EmailAddressListInterface $list
	 * @return EmailMetadataInterface
	 */
	public function withReplyTo(EmailAddressListInterface $list) : EmailMetadataInterface;
	
	/**
	 * Retrieves the to email address of this message. This data is taken out
	 * from the TO header of the message.
	 * 
	 * @return EmailAddressListInterface
	 */
	public function getTo() : EmailAddressListInterface;
	
	/**
	 * Returns an instance with the email address as To.
	 * 
	 * @param EmailAddressListInterface $list the email address of the receiver
	 * @return EmailMetadataInterface
	 */
	public function withTo(EmailAddressListInterface $list) : EmailMetadataInterface;
	
	/**
	 * Retrieves the CC email address of this message. This data is taken out
	 * from the CC header of the message.
	 * 
	 * @return EmailAddressListInterface
	 */
	public function getCc() : EmailAddressListInterface;
	
	/**
	 * Returns an instance with the specified email address as copy.
	 * 
	 * @param EmailAddressListInterface $list
	 * @return EmailMetadataInterface
	 */
	public function withCc(EmailAddressListInterface $list) : EmailMetadataInterface;
	
	/**
	 * Retrieves the BCC email address of this message. This data is taken out
	 * from the BCC header of the message.
	 * 
	 * @return EmailAddressListInterface
	 */
	public function getBcc() : EmailAddressListInterface;
	
	/**
	 * Returns an instance with the email address as Bcc.
	 * 
	 * @param EmailAddressListInterface $list
	 * @return EmailMetadataInterface
	 */
	public function withBcc(EmailAddressListInterface $list) : EmailMetadataInterface;
	
	/**
	 * Retrieves the reception date of this email.
	 * 
	 * The reception date is the date when the server that currently holds the
	 * message received it from its relays.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getReceptionDate() : ?DateTimeInterface;
	
	/**
	 * Returns an instance with the specified reception date.
	 * 
	 * @param DateTimeInterface $received
	 * @return EmailMetadataInterface
	 */
	public function withReceptionDate(DateTimeInterface $received) : EmailMetadataInterface;
	
	/**
	 * Retrieves the subject of the message.
	 * 
	 * @return string the subject
	 */
	public function getSubject() : string;
	
	/**
	 * Returns an instance with the specified subject.
	 * 
	 * @param string $subject the subject of the message
	 * @return EmailMetadataInterface
	 */
	public function withSubject(string $subject) : EmailMetadataInterface;
	
}
